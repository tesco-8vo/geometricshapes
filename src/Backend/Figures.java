package Backend;

/**
 *
 * @author anmijurane
 */
public class Figures {

    /*Triangulo Rectangulo*/
    public String Desc_T_Rectangulo() {
        String DescTRect = "Se denomina triángulo rectángulo a cualquier "
                + "triángulo con un ángulo recto, es decir, un ángulo de 90 "
                + "grados.  Las razones entre las longitudes de los lados de un"
                + " triángulo rectángulo es un enfoque de la trigonometría plana.";        
        return DescTRect;
    }
    
    public String FPeri_T_Rectangulo() {
        String FormPerimetro = "P = L + L + L";
        return FormPerimetro;
    }
    
    public String FArea_T_Rectangulo(){
        String FormArea = "A = b * h / 2";
        return FormArea;
    }
    
    /*Triangulo Isosceles*/    
    public void T_Isosceles() {
        
    }

    public void T_Escaleno() {
        
    }

    public void T_Obtusangulo() {
        
    }

    public void Acutangulo() {
        
    }
}
