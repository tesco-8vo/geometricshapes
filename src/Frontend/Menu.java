package Frontend;
import javax.swing.JOptionPane;

public class Menu extends javax.swing.JFrame {

    public Menu() {
        initComponents();
    }
    
    final String Primaria6 = "6° Primaria";
    final String Secundaria1 = "1° Secundaria";
    final String Secundaria2 = "2° Secundaria";
    Backend.Validations val = new Backend.Validations();

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnSexto = new javax.swing.JButton();
        BtnSegundo = new javax.swing.JButton();
        BtnPrimero = new javax.swing.JButton();
        BtnExit = new javax.swing.JButton();
        getName = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        GoIndex = new javax.swing.JMenuItem();
        FileExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        BtnSexto.setBackground(new java.awt.Color(69, 177, 69));
        BtnSexto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Frontend/Images/Menu/6 Primaria.png"))); // NOI18N
        BtnSexto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSextoActionPerformed(evt);
            }
        });
        getContentPane().add(BtnSexto, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 230, -1, -1));

        BtnSegundo.setBackground(new java.awt.Color(69, 177, 69));
        BtnSegundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Frontend/Images/Menu/2 de Secundaria.png"))); // NOI18N
        BtnSegundo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSegundoActionPerformed(evt);
            }
        });
        getContentPane().add(BtnSegundo, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 230, 80, 80));

        BtnPrimero.setBackground(new java.awt.Color(69, 177, 69));
        BtnPrimero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Frontend/Images/Menu/1 de Secundaria.png"))); // NOI18N
        BtnPrimero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrimeroActionPerformed(evt);
            }
        });
        getContentPane().add(BtnPrimero, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 230, -1, -1));

        BtnExit.setBackground(new java.awt.Color(69, 177, 69));
        BtnExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Frontend/Images/Menu/Exit.png"))); // NOI18N
        BtnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnExitActionPerformed(evt);
            }
        });
        getContentPane().add(BtnExit, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));
        getContentPane().add(getName, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 124, 470, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Frontend/Images/Menu/Portada.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 414));

        jMenu1.setText("Archivo");

        GoIndex.setText("Inicio");
        GoIndex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GoIndexActionPerformed(evt);
            }
        });
        jMenu1.add(GoIndex);

        FileExit.setText("Salir");
        FileExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FileExitActionPerformed(evt);
            }
        });
        jMenu1.add(FileExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Acerca");

        jMenuItem2.setText("de Nosotros");
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void FileExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FileExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_FileExitActionPerformed

    private void BtnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_BtnExitActionPerformed

    private void GoIndexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GoIndexActionPerformed
        Index windows = new Index();
        windows.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_GoIndexActionPerformed

    private void BtnSextoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSextoActionPerformed

        if(val.ValidCad()){
            //JOptionPane.showMessageDialog(null, "Entraste a Sexto de Primaria6");
            Category acc = new Category(getName.getText(), Primaria6);
            acc.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnSextoActionPerformed

    private void BtnPrimeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrimeroActionPerformed
        if(val.ValidCad()){
            //JOptionPane.showMessageDialog(null, "Entraste a 1° de Secundaria");
            Category acc = new Category(getName.getText(), Secundaria1);
            acc.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BtnPrimeroActionPerformed

    private void BtnSegundoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSegundoActionPerformed
        if(val.ValidCad()){            
            Category acc = new Category(getName.getText(), Secundaria2);
            acc.setVisible(true);
            this.dispose();
        }

    }//GEN-LAST:event_BtnSegundoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menu().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnExit;
    private javax.swing.JButton BtnPrimero;
    private javax.swing.JButton BtnSegundo;
    private javax.swing.JButton BtnSexto;
    private javax.swing.JMenuItem FileExit;
    private javax.swing.JMenuItem GoIndex;

    public static javax.swing.JTextField getName;

    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    // End of variables declaration//GEN-END:variables
}
